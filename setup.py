#!/usr/bin/env python3

from setuptools import setup
from importlib.machinery import SourceFileLoader

debtags = SourceFileLoader("debtags","./debtags").load_module()

setup(name='debtags',
      description="Debian Package Tags support tools",
      author="Enrico Zini",
      author_email="enrico@debian.org",
      url="http://debtags.debian.net",
      #install_requires=["debian", "apt"],
      license="GPL",
      version=debtags.VERSION,
      scripts=["debtags"],
      #test_suite="test",
)
