#!/usr/bin/python3

import os
import subprocess
import unittest

class TestPyflakesClean(unittest.TestCase):
    """ ensure that the tree is pyflakes clean """

    def test_pyflakes_clean(self):
        basedir = os.path.dirname(__file__)
        if not basedir:
            basedir=os.getcwd()
        basedir = os.path.abspath(os.path.join(basedir, ".."))
        self.assertEqual(subprocess.call(["pyflakes3", basedir]), 0)

if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
